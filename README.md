# Fedipolitik: Allgemeine Infos

Diese Datei beinhaltet eine Auflistung von Fediverse-Profilen politischer Entitäten (Parteien, Mandatsträger:innen, Amtsträger:innen, Behörden, offizielle Kanditat:innen für Wahlen, "wichtige Einzelpersonen").

Die Datei kann und soll kollektiv erweitert und aktualisiert werden. Möglichkeiten:

- Pull-Request
- Issue
- Nachricht an aktuelle(n) Maintainer:
  - https://social.tchncs.de/@cark
- Antwort auf: https://social.tchncs.de/@cark/105650620879066525 (Original-Toot).

Anspruch auf Vollständigkeit [uder](https://cknoll.github.io/uder) Korrektheit wird explizit negiert. Eine Erwähnung hier ist nicht gleichzusetzen mit inhaltlicher oder formaler Zustimmung oder Ablehnung. Sortiert wird innerhab der einzelnen Abschnitte grundsätzlich alphabetisch.

Diese Datei ist unter einer Creative Commons [CC-BY-SA 4.0 Lizenz](https://creativecommons.org/licenses/by-sa/4.0/legalcode.de) für die Weiterverbreitung und Nutzung freigegeben.

---

# Politische Fediverse-Profile

## Parteien und Untergliederungen

### Union

#### Christlich Demokratische Union Deutschlands (CDU)

| Wer                        | Link                                |
| :------------------------- | :---------------------------------- |
| CDU-Bezirksverband Kappeln | https://mastodon.social/@cdukappeln |

#### Christlich-Soziale Union (CSU)

Zum jetzigen Zeitpunkt sind keine Fediverse-Präsenzen der CSU oder ihrer Gliederungen bekannt.

### Sozialdemokratische Partei Deutschlands (SPD)

| Instanzen        | Link                   |
| :--------------- | :--------------------- |
| Mastodon-Instanz | https://sozen.network/ |
| Mastodon-Instanz | https://spd.social/    |

| Wer o. Was?                 | Link                                      |
| :-------------------------- | :---------------------------------------- |
| SPD Dudweiler               | https://sozen.network/@SPD_Dudweiler      |
| SPD-Bundesvorstand `(Bot)`  | https://sozen.network/@bundesvorstand_bot |
| SPD-Fraktion Bayern `(Bot)` | https://sozen.network/@spdfraktion_bayern |
| SPD Freundeskreis Italien   | https://spd.social/users/spditalien       |

### Freie Demokratische Partei Deutschlands (FDP)

Zum jetzigen Zeitpunkt sind keine Fediverse-Präsenzen der FDP oder ihrer Gliederungen bekannt.

### BÜNDNIS 90/DIE GRÜNEN (B'90/Grüne)

| Instanzen         | Link                                |
| :---------------- | :---------------------------------- |
| Mastodon-Instanz  | https://gruene.social/              |
| Pixelfed-Instanz  | https://pixel.gruene.social/        |
| Mobilizon-Instanz | https://events.gruene.social/       |
| PeerTube-Instanz  | https://peertube.netzbegruenung.de/ |

| Wer o. Was?                                                       | Link                                        |
| :---------------------------------------------------------------- | :------------------------------------------ |
| BÜNDNIS 90/DIE GRÜNEN Bonn: Arbeitskreis Digitaler Wandel         | https://bonn.social/@AKDiWa                 |
| BÜNDNIS 90/DIE GRÜNEN Landtagsfraktion Brandenburg                | https://gruene.social/@grueneltbb           |
| BÜNDNIS 90/DIE GRÜNEN Kreisverband Treptow-Köpenick               | https://gruene.social/@gruene_tk            |
| BÜNDNIS 90/DIE GRÜNEN Ortsverband München Hadern                  | https://gruene.social/@gruenemuenchenhadern |
| BÜNDNIS 90/DIE GRÜNEN Kreisverband Börde (Sachsen-Anhalt)         | https://gruene.social/@boerde               |
| BÜNDNIS 90/DIE GRÜNEN in der Samtgemeinde Harsefeld               | https://gruene.social/@harsefeld            |
| BÜNDNIS 90/DIE GRÜNEN im Bezirksausschuss 3 (München) Maxvorstadt | https://gruene.social/@maxvorstadt          |
| BÜNDNIS 90/DIE GRÜNEN Stadthagen                                  | https://gruene.social/@Stadthagen           |
| BÜNDNIS 90/DIE GRÜNEN Urbach                                      | https://gruene.social/@urbach               |

### DIE LINKE

| Instanzen                             | Link                            |
| :------------------------------------ | :------------------------------ |
| Mastodon-Instanz (Bundestagsfraktion) | https://social.linksfraktion.de |
| Mastodon-Instanz (Partei)             | https://linke.social            |

| Wer o. Was?                                                  | Link                                  |
| :----------------------------------------------------------- | :------------------------------------ |
| Linksfraktion in der Bezirksverodnetenversammlung von Pankow | https://mastodon.social/@linkebvvp    |
| DIE LINKE Kreisverband Saale-Holzland-Kreis                  | https://mastodon.social/@dielinke_shk |

#### Bundestagsfraktion

Die Linksfraktion im Bundestag betreibt eine eigene [Mastodon-Instanz](https://social.linksfraktion.de), auf der alle Abgeordneten einen Account haben. Bei vielen handelt es sich dabei (noch) um (offizielle) Bots, die auch entsprechend gekennzeichnet sind. Eine Übersicht findet man im [Profilverzeichnis der Instanz](https://social.linksfraktion.de/explore).

| Wer o. Was?                | Link                                                    |
| :------------------------- | :------------------------------------------------------ |
| Linksfraktion im Bundestag | https://social.linksfraktion.de/@linksfraktion          |
| Dietmar Bartsch            | https://social.linksfraktion.de/@dietmarbartsch         |
| Amira Mohamed Ali          | https://social.linksfraktion.de/@amira_mohamed_ali      |
| Jan Korte                  | https://social.linksfraktion.de/@jankorte               |
| Anke Domscheit-Berg        | https://social.linksfraktion.de/@ankedb                 |
| Janine Wissler             | https://social.linksfraktion.de/@janine_wissler         |
| Gökay Akbulut              | https://social.linksfraktion.de/@goekayakbulut          |
| Ali Al-Dailami             | https://social.linksfraktion.de/@ali_aldailami          |
| Matthias W. Birkwald       | https://social.linksfraktion.de/@matthias_w_birkwald    |
| Clara Bünger               | https://social.linksfraktion.de/@clarabuenger           |
| Sevim Dagdelen             | https://social.linksfraktion.de/@sevimdagdelen          |
| Klaus Ernst                | https://social.linksfraktion.de/@klausernst             |
| Susanne Ferschl            | https://social.linksfraktion.de/@susanneferschl         |
| Nicole Gohlke              | https://social.linksfraktion.de/@nicolegohlke           |
| Christian Görke            | https://social.linksfraktion.de/@christiangoerke        |
| Ates Gürpinar              | https://social.linksfraktion.de/@ates_guerpinar         |
| Gregor Gysi                | https://social.linksfraktion.de/@gregorgysi             |
| André Hahn                 | https://social.linksfraktion.de/@andrehahn              |
| Susanne Hennig-Wellsow     | https://social.linksfraktion.de/@susanne_hennig_wellsow |
| Sahra Wagenknecht          | https://social.linksfraktion.de/@sahra_wagenknecht      |
| Andrej Hunko               | https://social.linksfraktion.de/@andrejhunko            |
| Ina Latendorf              | https://social.linksfraktion.de/@ina_latendorf          |
| Caren Lay                  | https://social.linksfraktion.de/@carenlay               |
| Ralph Lenkert              | https://social.linksfraktion.de/@ralphlenkert           |
| Christian Leye             | https://social.linksfraktion.de/@christianleye          |
| Gesine Lötzsch             | https://social.linksfraktion.de/@gesine_loetzsch        |
| Thomas Lutze               | https://social.linksfraktion.de/@thomaslutze            |
| Pascal Meiser              | https://social.linksfraktion.de/@pascal_meiser          |
| Cornelia Möhring           | https://social.linksfraktion.de/@cornelia_moehring      |
| Zaklin Nastic              | https://social.linksfraktion.de/@zaklin_nastic          |
| Petra Pau                  | https://social.linksfraktion.de/@petra_pau              |
| Sören Pellmann             | https://social.linksfraktion.de/@soeren_pellmann        |
| Victor Perli               | https://social.linksfraktion.de/@victorperli            |
| Heidi Reichinnek           | https://social.linksfraktion.de/@heidi_reichinnek       |
| Martina Renner             | https://social.linksfraktion.de/@martina_renner         |
| Bernd Riexinger            | https://social.linksfraktion.de/@bernd_riexinger        |
| Petra Sitte                | https://social.linksfraktion.de/@petra_sitte            |
| Jessica Tatti              | https://social.linksfraktion.de/@jessica_tatti          |
| Alexander Ulrich           | https://social.linksfraktion.de/@alexander_ulrich       |
| Kathrin Vogler             | https://social.linksfraktion.de/@kathrinvogler          |

### Die PARTEI

| Instanzen        | Link                      |
| :--------------- | :------------------------ |
| Mastodon-Instanz | https://die-partei.social |

| Wer o. Was?                        | Link                                        |
| :--------------------------------- | :------------------------------------------ |
| Die PARTEI LV Berlin               | https://mastodon.social/@DiePARTEIBerlin    |
| Die PARTEI KV Erding               | https://die-partei.social/@DiePARTEI_Erding |
| Die PARTEI KV Duderstadt           | https://die-partei.social/@duderstadt       |
| Die PARTEI KV Hannover             | https://hannover.social/@diepartei          |
| Die PARTEI OV Kronshagen           | https://norden.social/@PARTEI_Kronshagen    |
| Die PARTEI KV Landau/Süd. Weinstr. | https://die-partei.social/@parteilandau     |
| Die PARTEI KV Mönchengladbach      | https://die-partei.social/@dieparteimg      |
| Die PARTEI KV Münster              | https://mastodon.social/@Die_PARTEI_MS      |
| Die PARTEI KV Nordsachsen          | https://die-partei.social/@nordsachsies     |
| Die PARTEI LV Niedersachsen        | https://die-partei.social/@niedersachsen    |
| Die PARTEI KV Nordwestmecklenburg  | https://die-partei.social/@diePARTEI_NWM    |
| Die PARTEI KV Pinneberg            | https://die-partei.social/@diepartei_pi     |
| Die PARTEI KV Reutlingen           | https://die-partei.social/@DiePARTEI_RT     |
| Die PARTEI KV Saalfeld-Rudolstadt  | https://die-partei.social/@DiePARTEI_SLF_RU |
| Die PARTEI Steiermark              | https://graz.social/@diePARTEI_stmk         |
| Die PARTEI Wien                    | https://wien.rocks/@diePARTEI               |

### Partei der Humanisten

| Wer o. Was?                  | Link                                          |
| :--------------------------- | :-------------------------------------------- |
| Partei der Humanisten Berlin | https://eupublic.social/@DieHumanisten_Berlin |

### Piratenpartei

| Instanzen         | Link                           |
| :---------------- | :----------------------------- |
| Friendica-Instanz | https://pirati.ca/             |
| Mastodon-Instanz  | https://piraten-partei.social/ |

| Wer o. Was?                                                           | Link                                                                  |
| :-------------------------------------------------------------------- | :-------------------------------------------------------------------- |
| Piratenpartei Bonn                                                    | https://bonn.social/@piratenbonn                                      |
| Piratenpartei Baden-Württemberg (BaWü / BW)                           | https://mastodon.cloud/@PiratenBW                                     |
| Piratenpartei Bund (inaktiv)                                          | https://mastodon.partipirate.org/@Piratenpartei                       |
| Piratenpartei Dresden                                                 | https://pirati.ca/profile/piratendresden                              |
| Piratenpartei Dresden (PeerTube)                                      | https://video.dresden.network/accounts/piraten_dresden/video-channels |
| Piratenpartei gem. Kreisverband der Kreise Main-Taunus und Hochtaunus | https://mastodon.social/@taunuspiraten                                |
| Piratenpartei Ortsverband Dresden-Neustadt                            | https://dresden.network/@neustadtpiraten                              |
| Piratenpartei Rhein-Erft                                              | https://pirati.ca/profile/piratenrheinerft                            |
| Piratenpartei Saarland                                                | https://mastodon.social/@piraten_saar                                 |
| Piratige Hochschulgruppe in Dresden                                   | https://chaos.social/@hopidd                                          |

### Sonstige

| Wer o. Was?   | Link                                        |
| :------------ | :------------------------------------------ |
| Die Sonstigen | https://climatejustice.social/@diesonstigen |

### DiEM25/MERA25

|  Wer o. Was?  | Link                                  |
| :-----------: | :------------------------------------ |
| DiEM25 Berlin | https://eupublic.social/@diem25berlin |
|    MERA25     | https://digitalcourage.social/@mera25 |

### Klimaliste

| Wer o. Was?                  | Link                                                  |
| :--------------------------- | :---------------------------------------------------- |
| Klimaliste Baden-Württemberg | https://climatejustice.social/@KlimalisteBW           |
| Klimaliste Bundespartei      | https://climatejustice.social/@Klimaliste_Deutschland |
| Klimafreunde Köln            | https://social.tchncs.de/@klima_freunde               |
| Klimaliste Leverkusen        | https://climatejustice.social/@klimalisteLEV          |
| Klimaliste Rems-Murr-Kreis   | https://climatejustice.social/@klimaliste_rmk         |
| Klimaliste Sachsen-Anhalt    | https://climatejustice.social/@KlimalisteST           |

### Volt

| Instanzen         | Link                           |
| :---------------- | :----------------------------- |
| Mastodon-Instanz  | https://volt.cafe/             |

| Wer o. Was?    | Link                              |
| :------------- | :-------------------------------- |
| Volt LV Bremen | https://norden.social/@voltbremen |

## Politische Jugendorganisationen

### Grüne Jugend

| Wer o. Was?                              | Link                                  |
| :--------------------------------------- | :------------------------------------ |
| Grüne Jugend Netzpolitik                 | https://gruene.social/@gjnetzpolitik  |
| Grüne Jugend Brandenburg                 | https://gruene.social/@gj_bb          |
| Grüne Jugend Darmstadt/Darmstadt-Dieburg | https://gruene.social/@gjdadi         |
| Grüne Jugend Karlsruhe                   | https://social.tchncs.de/@GJKarlsruhe |

## Behörden, Ämter und Parlamente

### EU-Behörden

| Instanzen        | Link                              |
| :--------------- | :-------------------------------- |
| Mastodon-Instanz | https://social.network.europa.eu/ |
| PeerTube-Instanz | https://tube.network.europa.eu/   |

#### Mastodon

| Wer o. Was?                                              | Link                                              |
| :------------------------------------------------------- | :------------------------------------------------ |
| EU-Kommision                                             | https://social.network.europa.eu/@EU_Commission   |
|                                                          | https://social.network.europa.eu/@EC_DIGIT        |
| Europäischer Datenschutzausschuss                        | https://social.network.europa.eu/@EDPS            |
| Europäischer Datenschutzbeauftragter                     | https://social.network.europa.eu/@EDPS_supervisor |
| Europäischer Gerichtshof (EuGH)                          | https://social.network.europa.eu/@Curia           |
| Agentur der Europäischen Union für Grundrechte           | https://social.network.europa.eu/@FRA             |
| Amt der Europäischen Union für geistiges Eigentum        | https://social.network.europa.eu/@EUIPO           |
| Eine Initiative der Europäischen Kommission - GD CONNECT | https://social.network.europa.eu/@EC_NGI          |
| Europäisches Übersetzungszentrum                         | https://social.network.europa.eu/@CDT             |
| Agentur der Europäischen Union für das Raumfahrtprogramm | https://social.network.europa.eu/@EUSPA           |
| European Commission Open Source Programme Office         | https://social.network.europa.eu/@EC_OSPO         |

### Ministerien und Bundesbehörden

| Instanzen        | Link                    |
| :--------------- | :---------------------- |
| Mastodon-Instanz | https://social.bund.de/ |

| Wer o. Was?                                                                             | Link                                    |
| :-------------------------------------------------------------------------------------- | :-------------------------------------- |
| Auswärtiges Amt                                                                         | https://social.bund.de/@AuswaertigesAmt |
| Bundesbeauftragte/r für den Datenschutz und die Informationsfreiheit (BfDI)             | https://social.bund.de/@bfdi            |
| Bundesamt für Sicherheit in der Informationstechnik (BSI)                               | https://social.bund.de/@bsi             |
| Stiftung Datenschutz                                                                    | https://social.bund.de/@DS_Stiftung     |
| Föderale IT-Kooperation                                                                 | https://social.bund.de/@FITKOfoederal   |
| Konferenz der unabhängigen Datenschutzaufsichtsbehörden des Bundes und der Länder (DSK) | https://social.bund.de/@dsk             |
| Presse- und Informationsamt der Bundesregierung (Bundespresseamt; BPA)                  | https://social.bund.de/@Bundespresseamt |
| Bundesministerium für Bildung und Forschung (BMBF)                                      | https://social.bund.de/@bmbf_bund       |
| Bundesministerium des Inneren (BMI)                                                     | https://social.bund.de/@bmi             |
| Informationstechnikzentrum Bund (ITZBund)                                               | https://social.bund.de/@itzbund         |
| Bundesamt für Strahlenschutz                                                            | https://social.bund.de/@strahlenschutz  |
| Sächsische Datenschutzbeauftragte                                                       | https://social.bund.de/@sdb             |
| Zoll                                                                                    | https://social.bund.de/@Zoll            |
| Bundesanstalt Technisches Hilfswerk (THW)                                               | https://social.bund.de/@THW             |
| Bundesagentur für Arbeit (BA)                                                           | https://mastodon.social/@Bundesagentur  |

#### inoffiziell

| Wer o. Was?                                                                                                              | Link                                  |
| :----------------------------------------------------------------------------------------------------------------------- | :------------------------------------ |
| FediNINA - Informationen des Bundesamtes für Bevölkerungsschutz und Katastrophenhilfe im Fediverse `(Inoffizielle Bots)` | https://social.prepedia.org/@FediNINA |

### Berlin

| Wer o. Was?                                                            | Link                           |
| :--------------------------------------------------------------------- | :----------------------------- |
| Berliner Beauftragte für Datenschutz und Informationsfreiheit (BlnBDI) | https://social.bund.de/@BlnBDI |

### Baden-Württemberg

| Instanzen        | Link                     |
| :--------------- | :----------------------- |
| Mastodon-Instanz | https://bawü.social/     |
| PeerTube-Instanz | https://tube.bawü.social |

| Wer o. Was?                                                                                                       | Link                                          |
| :---------------------------------------------------------------------------------------------------------------- | :-------------------------------------------- |
| Landesregierung des Landes Baden-Württemberg (LReg BW / BaWü)                                                     | https://mastodon.social/@RegierungBW          |
| Ministerium für Umwelt, Klima und Energiewirtschaft Baden-Württemberg (UM BaWü / BW)                              | https://bawü.social/@Umweltministerium        |
| Regierungspräsidium Freiburg                                                                                      | https://bawü.social/@RPFreiburg               |
| Pressestelle des Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü) | https://bawü.social/@lfdi_pressestelle        |
| Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü)                  | https://bawü.social/@lfdi                     |
| Pressestelle des Landesbeauftragten für den Datenschutz und die Informationsfreiheit (LfDI BaWü)                  | https://tube.bawü.social/a/lfdi_pressestelle/ |
| Landeszentrale für politische Bildung Baden-Württemberg (LpB BW / BaWü)                                           | https://xn--baw-joa.social/@lpb               |

### Rheinland-Pfalz

| Wer o. Was?             | Link                          |
| :---------------------- | :---------------------------- |
| Landtag Rheinland-Pfalz | https://social.bund.de/@ltrlp |

### Hessen

| Wer o. Was?        | Link                                      |
| :----------------- | :---------------------------------------- |
| Hessischer Landtag | https://social.bund.de/@HessischerLandtag |

### Nordrhein-Westfalen

| Wer o. Was?                               | Link                           |
| :---------------------------------------- | :----------------------------- |
| Landeszentrale für politische Bildung NRW | https://bildung.social/@lpbnrw |

## Amtsträger:innen

| Wer                                           | Link                                |
| :-------------------------------------------- | :---------------------------------- |
| Landesdigitalminister SH Jan Philipp Albrecht | https://gruene.social/@janalbrecht/ |
| Prof. Ulrich Kelber (BfDI)                    | https://bonn.social/@ulrichkelber   |

## Mandatsträger:innen

| Mitglieder des Europäischen Parlaments (deutschsprachig) | Link                               |
| :------------------------------------------------------- | :--------------------------------- |
| Patrick Breyer                                           | https://chaos.social/@echo_pbreyer |
| Tiemo Wölken                                             | https://troet.cafe/@woelken/       |

| Mitglieder des Bundestages   | Link                                  |
| :--------------------------- | :------------------------------------ |
| Maik Außendorf (Grüne)       | https://gruene.social/@AussenMa       |
| Marco Bülow (SPD, 2002-2021) | https://ruhr.social/@marcobuelow      |
| Saskia Esken (SPD)           | https://mastodon.social/@eskensaskia  |
| Kevin Kühnert (SPD)          | https://mastodon.social/@kuehnikev    |
| Zoe Mayer (Grüne)            | https://gruene.social/@zoe            |
| Konstantin von Notz (Grüne)  | https://gruene.social/@KonstantinNotz |
| Tabea Rößner (Grüne)         | https://gruene.social/@TabeaRoessner  |
| Margit Stumpp (Grüne)        | https://gruene.social/@MargitStumpp   |
| Katrin Göring-Eckardt        | https://gruene.social/@GoeringEckardt |

Die Mitglieder der Fraktion DIE LINKE im Bundestag sind der Übersichtlichkeit halber oben unter „DIE LINKE“ aufgeführt.

| Mitglieder eines Landtages | Link                                   |
| :------------------------- | :------------------------------------- |
| Marie Schäffer             | https://gruene.social/@marie_schaeffer |

| Mitglieder/Fraktionen eines Kommunalparlamentes             | Link                                   |
| :---------------------------------------------------------- | :------------------------------------- |
| Stefan Borggraefe (Rat der Stadt Witten)                    | https://mastodon.social/@BorgTenOfNine |
| Daniel Gaittet (Stadtrat Regensburg, BÜNDNIS 90/DIE GRÜNEN) | https://gruene.social/@dgaittet        |
| DISSIDENTEN-Fraktion im Dresdener Stadtrat                  | https://dresden.network/@dissidentenDD |
| Lukas Weidinger (Stadtrat Würzburg, BÜNDNIS 90/DIE GRÜNEN)  | https://gruene.social/@lukasweidinger  |
| Dr. Martin Schulte-Wissermann (Stadtrat Dresden, Piraten)   | https://dresden.network/@mswdresden    |
| Sonja Lemke (Stadtrat Dortmund, Die Linke)                  | https://ruhr.social/@sonjalemke        |

## Weitere Personen des politischen Lebens

| Wer                       | Relevanz                                                        | Link                                 |
| :------------------------ | :-------------------------------------------------------------- | :----------------------------------- |
| Stephanie Henkel (ÜckÜck) | Bundestagswahlkandidatin 2021                                   | https://dresden.network/@ueckueck    |
| Malte Spitz               | Parteirat Bündnis '90/Die Grünen                                | https://gruene.social/@maltespitz    |
| Ruprecht Polenz           | ehemaliges Mitglied des Bundestages für die CDU                 | https://social.dev-wiki.de/@polenz_r      |
| Barend Wolf               | LV Die Humanisten Berlin                                        | https://eupublic.social/@barend_wolf |
| Felix Reda                | ehemaliges Mitglied des europäischen Parlaments für die Piraten | https://chaos.social/@senficon       |

## Deutschsprachiges Ausland

### ÖVP

| Wer o. Was? | Link                                  |
| :---------- | :------------------------------------ |
| Georg Ecker | https://social.wien.rocks/@georgecker |

## Parteigründungsinitiativen

| Wer                             | Link                      |
| :------------------------------ | :------------------------ |
| Partei für Lebensqualität (PLQ) | https://legal.social/@plq |

## Überregionale Nichtregierungsorganisationen & Projekte

| Wer                                              | Link                                              |
| :----------------------------------------------- | :------------------------------------------------ |
| Amnesty Digital DE                               | https://social.tchncs.de/@amnesty_digital_de      |
| Bedingungsloses Grundeinkommen (BGE/UBI) Fanclub | https://social.tchncs.de/@BGE_Fanclub             |
| BUNDjugend `(Bot)`                               | https://botsin.space/@BUNDjugend                  |
| BUNDjugend (Arbeitskreis Digitalisierung)        | https://climatejustice.global/@BUNDjugend_ak_digi |
| Digitalcourage e. V.                             | https://digitalcourage.social/@digitalcourage     |
| Ende Gelände                                     | https://climatejustice.global/@ende_gelaende      |
| Fridays for Future                               | https://chaos.social/@fff                         |
| Frag den Staat                                   | https://chaos.social/@fragdenstaat                |
| Free Software Foundation Europe                  | https://mastodon.social/@fsfe                     |
| Greenpeace `(Bot)`                               | https://newsbots.eu/@Greenpeace                   |
| Junge Europäische Föderalisten Rheinland-Pfalz   | https://eupublic.social/@jefRLP                   |
| Junge Europäische Föderalisten Trier             | https://eupublic.social/@jef_trier                |
| Liberapay                                        | https://mastodon.xyz/@Liberapay                   |
| Linux User im Bereich der Kirchen e. V. (LUKI)   | https://kirche.social/@luki                       |
| Mobilsicher.de                                   | https://mastodontech.de/@mobilsicher              |
| NOYB - Europäisches Zentrum für digitale Rechte  | https://mastodon.social/@noybeu                   |
| Parents4Future                                   | https://climatejustice.global/@parents4future     |
| Privacy is not a Crime                           | https://mastodon.online/@PrivacyIsNotACrime       |
| Prototype Fund                                   | https://mastodon.social/@PrototypeFund            |
| XR Germany                                       | https://social.rebellion.global/@xrgermany        |

## Unsortierte Einträge

Gruppen oder individuen, die nicht in eine der obigen Kategorien passen, aber (bis zur Erstellung einer richtigen Datenbank) nicht ignoriert werden sollen

| Wer                                  | Link                               |
| :----------------------------------- | :--------------------------------- |
| Junge Europäische Föderalisten Trier | https://eupublic.social/@jef_trier |
